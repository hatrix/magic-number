#! /usr/bin/env python3

import sys
import os

# Magic numbers
types = dict()
types['jpg'] = {'ext': '.jpg', 'code': [0xff, 0xd8, 0xff]}
types['png'] = {'ext': '.png', 'code': [0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 
                                        0x1a, 0x0a]}
types['elf'] = {'ext': ''    , 'code': [0x7f, 0x45, 0x4c, 0x46]}
types['zip'] = {'ext': '.zip', 'code' :[0x50, 0x4b, 0x03, 0x04]}
types['pdf'] = {'ext': '.pdf', 'code' :[0x25, 0x50, 0x44, 0x46]}


# max size of magic number
SIZE = len(max(types.values(), key=lambda x: len(x['code']))['code'])

# Check for input file
if len(sys.argv) < 2:
    print("Please provide an input file.")
    exit()
elif not os.path.isfile(sys.argv[1]):
    print("Unable to locate file")
    exit()


# Open file and read first byte
f = open(sys.argv[1], 'rb')
byte = f.read(1)


offset = 8
number = -1
seq = []
started = False
while byte != b'':
    if len(seq) >= SIZE:
        seq = seq[1:]
    seq.append(ord(byte))
   
    # Iterate through the known types
    for magic, data in types.items():
        if len(seq) <= SIZE - len(data['code']):
            tmp = seq
        else:
            tmp = seq[SIZE - len(data['code']):]

        # If magic number found, write it!
        if tmp == data['code']:
            started = True
            number += 1
            
            print("{} found at offset {}, written to hidden_{}{}".format(
                  magic, offset - len(data['code']) * 8, number, data['ext']))
            

            r = open('hidden_{}{}'.format(number, data['ext']), 'wb')

            for val in tmp:
                r.write(bytes([val]))


            # Read one more byte because we're writing just after
            byte = f.read(1)
            break

    if started and byte != b'':
        r.write(bytes(byte))

    byte = f.read(1)
    offset += 8
